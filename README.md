##### `Editeur`
Ce site  est édité par l'Université  Lille
Cité scientifique
59655 Villeneuve d'Ascq Cedex

##### `Directeur de la Publication`
Monsieur Régis Bordet, Président de l'Université de Lille

##### `Webmestre`
Jean-Stéphane Varré, directeur du Département Informatique (FIL)

##### `Hébergement du site`
Faculté des Sciences et Technologies   
Département Informatique   
Formations Informatiques de Lille   
Université de Lille   
Bât. M5   
59655 Villeneuve d'Ascq Cedex   

##### `Conception / Réalisation`
[Gitlab CE](https://about.gitlab.com)

##### `Technologies utilisées`
[Gitlab CE](https://about.gitlab.com)

##### `Ligne éditoriale`
Le service communication de l'Université Lille

##### `Droits d'auteur et propriété intellectuelle`
L'ensemble de ce site relève de la législation française et internationale sur le droit d'auteur et la propriété intellectuelle. Tous les droits de reproduction sont réservés.
La reproduction de tout ou partie de ce site sur un support électronique quel qu'il soit est formellement interdite sauf autorisation expresse du directeur de la publication.
Les photographies, textes, dessins, images etc. protégés par les droits de la propriété intellectuelle sont la propriété de L'Université Lille ou de tiers ayant permis à l'Université Lille d'en faire usage.

##### `Avertissement concernant les informations disponibles sur ce site`
L'Université Lille s'efforce d'assurer au mieux de ses possibilités, l'exactitude et la mise à jour des informations diffusées sur ce site, dont elle se réserve le droit de corriger, à tout moment et sans préavis, le contenu. Toutefois, L'Université Lille ne peut garantir l'exactitude, la précision ou l'exhaustivité des informations mises à la disposition sur ce site. En conséquence, L'Université Lille décline toute responsabilité :

* pour toute interruption du site
* survenance de bogues
* pour toute inexactitude ou omission portant sur des informations disponibles sur le site
* pour tous dommages résultant d'une intrusion frauduleuse d'un tiers ayant entraîné une modification des informations mises à la disposition sur le site

##### `Liens hypertextes`
Le site peut inclure des liens vers d'autres sites Web ou d'autres sources Internet. Dans la mesure où l'Université Lille ne peut contrôler ces sites et ces sources externes, elle ne peut être tenue pour responsable de la mise à disposition de ces sites et sources externes, et ne peut supporter aucune responsabilité quant au contenu, publicités, produits, services ou tout autre matériel disponible sur ou à partir de ces sites ou sources externes. De plus, l'Université Lille ne pourra être tenu responsable de tous dommages ou pertes avérés ou allégués consécutifs ou en relation avec l'utilisation ou avec le fait d'avoir fait confiance au contenu, à des biens ou des services disponibles sur ces sites ou sources externes.

##### `Cookies / Témoins`
Les témoins servent à faciliter la navigation sur le site et à rationaliser les procédures d'enregistrement ou de mesure d'audience.   
Les témoins sont des fichiers texte placés sur le disque dur de votre ordinateur par un serveur de page Web. Votre ordinateur est sans doute configuré pour accepter les témoins. Vous pouvez cependant modifier ce paramètre pour qu'il les refuse et l'Université Lille vous suggère de le faire si l'utilisation des témoins vous inquiète.   
Quand vous visitez ce site Web, pour chercher, lire ou télécharger de l'information, l'Université Lille recueille et conserve certains "renseignements visiteur" vous concernant, tels le nom du domaine et de l'ordinateur hôte à partir desquels vous allez sur Internet, l'adresse du protocole Internet (IP) de l'ordinateur que vous utilisez, la date et l'heure de votre navigation sur ce site, et les URL à partir desquelles vous êtes passé à ce site. L'Université Lille utilise cette information pour analyser et mesurer la fréquentation de son site et pour travailler à le rendre plus utile. L'Université Lille détruit cette information après un certain temps. Si vous ne voulez pas que l'Université Lille recueille ce genre d'information, ne naviguez pas sur son site.
